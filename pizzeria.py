menu = {
	1: { "name": "Pizza",   "items": { 1: { "name": "Pepperoni", "price" : 9}, 2: {"name": "Margherita", "price": 8}, 3: {	"name": "Quatro Formagio", "price": 8.5}, 4: { "name": "Mexican", "price": 10}, 5: { "name": "Custom", "price": 7}	},},
	2: { "name": "Topping", "items": { 1: { "name": "Pepperoni", "price": .5}, 2: {"name": "Tuna", "price": .8}, 3: {"name": "Ham",	"price": .4}, 4: { "name": "Chicken", "price": .9}, 5: { "name": "Beef", "price": 1}, 6: { "name": "Chilli", "price": 1}, 7: { "name": "Pineapple", "price": 1} },},
	3: { "name": "Base",    "items": { 1: { "name": "Thin", "price": 1.5}, 2: { "name": "Normal", "price": 0} } },
	4: { "name": "Size",    "items": { 1: { "name": "Small", "price": 0}, 2: { "name": "Medium", "price": 3}, 3: {	"name": "Large", "price": 6} } },
	5: { "name": "Crust",   "items": { 1: { "name": "Stuffed", "price": 1}, 2: { "name": "Normal", "price": 0} } },
	6: { "name": "Extras",  "items": { 1: { "name": "Dough Balls", "price": 3}, 2: { "name": "Chips", "price": 2.5}, 3: { "name": "Soft Drink", "price": 2}, 4: { "name": "Goujons", "price": 4}, 5: { "name": "Dips", "price": 2} } } }
order = [0,0,0,0,0,0]
print("Pizzeria")
for a in range (1,len(menu)+1):
	if (a == 2 and userChoice != "5"): continue # we don't need the toppings menu if it's not a custom pizza
	else:
		print("\nSelect your " + menu[a]["name"] + ": ")
		for b in range(1,len(menu[a]["items"])+1):
			print(str(b) + " - " + "{:<15}".format(menu[a]["items"][b]["name"]) + "  " + "{:5.2f}".format(menu[a]["items"][b]["price"]) + " EUR")
		while(True):
			userChoice = input("\nYour choice: ")
			try:
				order[a-1]=+menu[a]["items"][int(userChoice)]["price"]
				break
			except ValueError: print("\n *** Incorrect option selected! ***")
			except KeyError: print("\n *** Incorrect option selected! ***")	
print("\nYour total bill is: " + "{:>6.2f}".format(sum(order)) + " EUR")